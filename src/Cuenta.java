/**
 * Created by andres.valle on 29/06/17.
 */
public class Cuenta {
    protected double balance;
    private Cliente[] propietarios;

    public double geBalance(){
        return balance;
    }

    public void deposito(double monto){
        balance += monto;
    }

    public void extraccion(double monto){
        balance -= monto;
    }
}
